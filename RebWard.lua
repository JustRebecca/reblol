local function createUI()
    rebWard = menu.label("Reb Ward")
    drawPosition = menu.checkbox("Draw Position", 0)
end

function round(num, numDecimalPlaces)
    local mult = 10 ^ (numDecimalPlaces or 0)
    return math.floor(num * mult + 0.5) / mult
end

local function drawWards()
    local wardPositions = {
        {x = 2976, y = 51, z = 8336}, -- b blue
        {x = 4024, y = 52, z = 6408}, -- b wolf
        {x = 3108, y = 50, z = 8946}, -- b j entrance
        {x = 7366, y = 52, z = 3304}, -- b red
        {x = 5970, y = 51, z = 6186}, -- b t1 mid
        {x = 11796, y = 51, z = 6556}, -- r blue
        {x = 7456, y = 49, z = 11636}, -- r red
        {x = 11750, y = 50, z = 5936}, -- r j entrance
        {x = 10780, y = 63, z = 8520}, -- r wolf
        {x = 8718, y = 54, z = 8666}, -- r t1 mid
        {x = 9396, y = -71, z = 5722}, -- pixel
        {x = 11786, y = -70, z = 4086}, -- bot river
        {x = 3184, y = -64, z = 10796}, -- top river
        {x = 5108, y = -71, z = 9166}, -- top tri
        {x = 4258, y = 48, z = 11868} -- top t1 bush

    }
    local wardWalls = {
        {x = 11722, y = -71, z = 4658}, --bot over wall1
        {x = 12222, y = 51, z = 5008}, --bot end pos
        {x = 10069, y = -71, z = 3919}, --drake tribush bot
        {x = 10272, y = 49, z = 3300} --drake tribush bot
    }
    for i = 1, #wardPositions do
        render.circle_3d(
            vec3:new(wardPositions[i].x, wardPositions[i].y, wardPositions[i].z),
            10,
            color:new(80, 220, 100)
        )
    end
    for i = 1, #wardWalls do
        render.circle_3d(vec3:new(wardWalls[i].x, wardWalls[i].y, wardWalls[i].z), 3, color:new(75, 139, 59))
    end
    if drawPosition:get_value() then
        local hero = object_manager.get_local()
        render.text(
            vec2:new(100, 50),
            "Position : X= " ..
                round(hero:get_position().x, 2) ..
                    " Y= " .. round(hero:get_position().y, 2) .. " Z= " .. round(hero:get_position().z, 2),
            15,
            color:new(255, 255, 255)
        )
    end
end

createUI()
register_callback("draw", drawWards)
